from django import forms
from django.core.exceptions import ValidationError

from .models import Question

class QuestionCreateForm(forms.ModelForm):

    choice_a = forms.CharField(
        label="Choice A",
        max_length=50,
        required=True,
        error_messages={
            'required': "Cant add question whitout choice"
        }
    )
    choice_b = forms.CharField(label="Choice B", max_length=50, required=False)
    choice_c = forms.CharField(label="Choice C", max_length=50, required=False)
    
    class Meta:
        model = Question
        fields = ("text", "publication_date",)
