import datetime

from django.db import models
from django.utils import timezone

class Question(models.Model):
    text = models.CharField(max_length=200)
    publication_date = models.DateTimeField("date published")

    def __str__(self):
        return self.text

    def was_published_recently(self):
        time = timezone.now() - datetime.timedelta(days=1)
        return timezone.now() >= self.publication_date >= time

class Choice(models.Model):
    question = models.ForeignKey(Question, null=True, blank=True, on_delete=models.SET_NULL)
    text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.text
