from django.views import generic
from django.utils import timezone
from django.urls import reverse

from .forms import QuestionCreateForm
from .models import Question

class IndexView(generic.ListView):
    template_name = "polls/index.html"
    model = Question
    
    def get_queryset(self):
        """Return last published questions"""
        return super().get_queryset().filter(
            publication_date__lte=timezone.now()
        ).order_by("-publication_date")


class QuestionDetailView(generic.DetailView):
    template_name = "polls/detail.html"
    model = Question

    def get_queryset(self):
        """
        Excludes any questions that arent published yet
        """
        return super().get_queryset().filter(publication_date__lte=timezone.now())


class QuestionCreateView(generic.CreateView):
    template_name = "polls/create.html"
    form_class = QuestionCreateForm
    model = Question

    def get_success_url(self):
        return reverse("polls:detail", kwargs={'pk': self.object.pk})
