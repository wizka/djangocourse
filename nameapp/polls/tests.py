import datetime

from django.http import HttpRequest
from django.test import TestCase
from django.utils import timezone
from django.urls.base import reverse

from .models import Question

### Software testing fundamentals
### Programacion
"""
1. Mas tests es mejor (Coverage).
2. Crear una clase para cada modelo, y para cada vista.
3. Los nombres de los metodos FULL DESCRIPTIVOS.
"""

#Model test
class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_questions(self):
        """Questions for the future, must return False"""
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question.objects.create(
            text='Test Question in the future',
            publication_date=time,
        )
        self.assertIs(future_question.was_published_recently(), False)


    def test_was_published_recently_with_now_questions(self):
        """Questions now published must return True"""
        time = timezone.now()
        future_question = Question.objects.create(
            text='Test Question now',
            publication_date=time,
        )
        self.assertIs(future_question.was_published_recently(), True)


    def test_was_published_recently_with_past_questions(self):
        """Questions published in the past must return False"""
        time = timezone.now() - datetime.timedelta(days=30)
        future_question = Question.objects.create(
            text='Test Question in the past',
            publication_date=time,
        )
        self.assertIs(future_question.was_published_recently(), False)


###ListView test
class QuestionIndexViewTests(TestCase):
    
    def test_no_questions(self):
        """If not questions registered, displayed appropiate message"""
        response = self.client.get(reverse("polls:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls registered.")
        self.assertQuerysetEqual(response.context['object_list'], [])


def create_question(text, days=0):
    time = timezone.now() - datetime.timedelta(days=days)
    return Question.objects.create(text=text,publication_date=time)


###DetailView
class QuestionDetailViewTest(TestCase):
    """
    The detail view of a question with a publication_date
    in the future returns 404 error not found
    """
    def test_future_question(self):
        future_question = create_question('future', days=30)
        url = reverse("polls:detail", kwargs={ "pk": future_question.pk, })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
    
    """If is published, show"""
    def test_past_question(self):
        past_question = create_question('past', days=-30)
        url = reverse("polls:detail", kwargs={ "pk": past_question.pk, })
        response = self.client.get(url)
        self.assertContains(response, past_question.text)


class QuestionCreateViewTest(TestCase):
    """
    CONTRAINTS
    1. Questions cannot be added whithout a choice
    """
    def test_create_question_without_choices(self):
        """
        Cannot create question without a choice
        Must be display message: Cant add question whitout choice...
        """
        time = timezone.now() - datetime.timedelta(days=-1)
        url = reverse("polls:create")
        data = { "text": "Question without", "publication_date": time }
        response = self.client.post(url, data)
        self.assertContains(response, "Cant add question whitout choice")


    def test_create_question_with_choices(self):
        """
        Cannot create question without a choice
        Must be display a list with questions, includes
        object created
        """
        time = timezone.now()
        url = reverse("polls:create")
        data = {
            "text": "Question with choices",
            "publication_date": time,
            "choice_a": "Option A",
            "choice_b": "Option B",
            "choice_c": "Option C",
        }
        response = self.client.post(url, data) ### Post
        self.assertContains(response, time)
