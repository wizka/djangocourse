from django.urls import path

from .views import (
    IndexView,
    QuestionCreateView,
    QuestionDetailView,
)


app_name='polls'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('<int:pk>', QuestionDetailView.as_view(), name='detail'),
    path('create', QuestionCreateView.as_view(), name='create'),
]
