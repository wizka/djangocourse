from django.contrib import admin

from .models import (
    Choice,
    Question,
)

class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fields = ["publication_date", "text"]
    inlines = [ChoiceInline]
    list_display = ("publication_date", "text", "was_published_recently")
    list_filter = ["publication_date"]
    search_fields = ["text"]


admin.site.register(Question, QuestionAdmin)
